import { getUpdatedAgedBrie, getUpdatedBackstagePass, getUpdatedSulphuras } from "./updateItem";

export class Item {
  name: string;
  sellIn: number;
  quality: number;

  constructor(name, sellIn, quality) {
    this.name = name;
    this.sellIn = sellIn;
    this.quality = quality;
  }
}

export class GildedRose {
  items: Array<Item>;

  constructor(items = [] as Array<Item>) {
    this.items = items;
  }

  updateQuality() {

    const refactoredItems: string[] = [
      'Aged Brie',
      'Sulfuras, Hand of Ragnaros',
      'Backstage passes to a TAFKAL80ETC concert',

      // TODO
      // 'Conjured Mana Cake',
      // 'Elixir of the Mongoose',
      // '+5 Dexterity Vest'
    ]

    for (let i = 0; i < this.items.length; i++) {

      /**
       * Use legacy logic to update items not yet refactored
       * 
       * Do Not edit legacy code
       * 
       */
      if(!refactoredItems.includes(this.items[i].name)){
        if (this.items[i].name != 'Aged Brie' && this.items[i].name != 'Backstage passes to a TAFKAL80ETC concert') {
          if (this.items[i].quality > 0) {
            if (this.items[i].name != 'Sulfuras, Hand of Ragnaros') {
              this.items[i].quality = this.items[i].quality - 1
            }
          }
        } else {
          if (this.items[i].quality < 50) {
            this.items[i].quality = this.items[i].quality + 1
            if (this.items[i].name == 'Backstage passes to a TAFKAL80ETC concert') {
              if (this.items[i].sellIn < 11) {
                if (this.items[i].quality < 50) {
                  this.items[i].quality = this.items[i].quality + 1
                }
              }
              if (this.items[i].sellIn < 6) {
                if (this.items[i].quality < 50) {
                  this.items[i].quality = this.items[i].quality + 1
                }
              }
            }
          }
        }
        if (this.items[i].name != 'Sulfuras, Hand of Ragnaros') {
          this.items[i].sellIn = this.items[i].sellIn - 1;
        }
        if (this.items[i].sellIn < 0) {
          if (this.items[i].name != 'Aged Brie') {
            if (this.items[i].name != 'Backstage passes to a TAFKAL80ETC concert') {
              if (this.items[i].quality > 0) {
                if (this.items[i].name != 'Sulfuras, Hand of Ragnaros') {
                  this.items[i].quality = this.items[i].quality - 1
                }
              }
            } else {
              this.items[i].quality = this.items[i].quality - this.items[i].quality
            }
          } else {
            if (this.items[i].quality < 50) {
              this.items[i].quality = this.items[i].quality + 1
            }
          }
        }
      }

      /**
       * New implementations for item updates go here...
       * 
       */
      if(refactoredItems.includes(this.items[i].name)){
        switch(this.items[i].name){
          case 'Aged Brie': {
            this.items[i] = getUpdatedAgedBrie(this.items[i]);
            break;
          }
          case 'Sulfuras, Hand of Ragnaros': {
            this.items[i] = getUpdatedSulphuras(this.items[i]);
            break;
          }
          case 'Backstage passes to a TAFKAL80ETC concert': {
            this.items[i] = getUpdatedBackstagePass(this.items[i]);
            break;
          }
          
          // TODO
          // ...add calls to more implementations here.

          default: {
            throw new Error(`No implementation found for ${this.items[i].name}. Make sure the name spelling is correct.`);
          }
        }
      }

    }

    return this.items;
  }
}
