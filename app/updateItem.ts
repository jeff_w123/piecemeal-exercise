import { Item } from './gilded-rose'

export const getUpdatedAgedBrie = (item: Item) :Item =>  {
    const updatedSellin = item.sellIn - 1;

    let updatedQuality = increaseQuality(item.quality);

    // do again if sellin is negative
    if(updatedSellin < 0) updatedQuality = increaseQuality(updatedQuality);

    return new Item(
        'Aged Brie', 
        updatedSellin,
        updatedQuality
    );
}

export const getUpdatedSulphuras = (item: Item) :Item =>  {
    
    // NOOP
    return new Item(
        'Sulfuras, Hand of Ragnaros', 
        item.sellIn,
        item.quality // assumes that the input is always 80, the requirements are not clear on this
    );
}

export const getUpdatedBackstagePass = (item: Item) :Item =>  {
    const updatedSellin = item.sellIn - 1;

    let updatedQuality = 0;
    if(item.sellIn > 0){
        updatedQuality = increaseQuality(item.quality);
        if(item.sellIn < 11) updatedQuality = increaseQuality(updatedQuality);
        if(item.sellIn < 6) updatedQuality = increaseQuality(updatedQuality);
    }

    return new Item(
        'Backstage passes to a TAFKAL80ETC concert', 
        updatedSellin,
        updatedQuality
    );
}

const increaseQuality = (quality: number) => quality < 50 ? quality + 1 : quality;