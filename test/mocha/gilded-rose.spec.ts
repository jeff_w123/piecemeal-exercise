import { expect } from 'chai';
import { Item, GildedRose } from '@/gilded-rose';

describe('Aged brie update', () => { 

  it('should decrease sell in by 1.', () => {
    const gildedRose = new GildedRose([ new Item('Aged Brie', 1, 1) ]);
    const items = gildedRose.updateQuality();
    const added =items[0]
    expect(added.sellIn).to.equal(0);
  });

  it('should increase quality by 1 when sell in is positive.', () => {
    const gildedRose = new GildedRose([ new Item('Aged Brie', 1, 1) ]);
    const items = gildedRose.updateQuality();
    const added =items[0]
    expect(added.quality).to.equal(2);
  });

  it('should increase quality to maximum of 50 when sell in is positive.', () => {
    const gildedRose = new GildedRose([ new Item('Aged Brie', 1, 50) ]);
    const items = gildedRose.updateQuality();
     const added =items[0]
    expect(added.quality).to.equal(50);
});

  it('should increase quality to maximum of 50 when sell in is negative.', () => {
      const gildedRose = new GildedRose([ new Item('Aged Brie', -1, 49) ]);
      const items = gildedRose.updateQuality();
       const added =items[0]
      expect(added.quality).to.equal(50);
  });

  it('should increase quality by 2 when sell in is negative.', () => {
      const gildedRose = new GildedRose([ new Item('Aged Brie', -10, 10) ]);
      const items = gildedRose.updateQuality();
       const added =items[0]
      expect(added.quality).to.equal(12);
  });
})

describe('Sulphuras update', () => { 

  it('should not change sell in.', () => {
    const gildedRose = new GildedRose([ new Item('Sulfuras, Hand of Ragnaros', 1, 80) ]);
    const items = gildedRose.updateQuality();
     const added =items[0]
    expect(added.sellIn).to.equal(1);
  });

  it('should not change quality.', () => {
    const gildedRose = new GildedRose([ new Item('Sulfuras, Hand of Ragnaros', 1, 80) ]);
    const items = gildedRose.updateQuality();
     const added =items[0]
    expect(added.quality).to.equal(80);
  });
})

describe('Backstage pass update', () => { 

  it('should decrease sell in by 1.', () => {
    const gildedRose = new GildedRose([ new Item('Backstage passes to a TAFKAL80ETC concert', 1, 1) ]);
    const items = gildedRose.updateQuality();
    const added =items[0]
    expect(added.sellIn).to.equal(0);
  });

  it('should increase quality by 1 when sell in is greater than 10.', () => {
    const gildedRose = new GildedRose([ new Item('Backstage passes to a TAFKAL80ETC concert', 11, 1) ]);
    const items = gildedRose.updateQuality();
    const added =items[0]
    expect(added.quality).to.equal(2);
  });

  // Test boundaries of range 6 to 10 day sell in
  it('should increase quality by 2 when sell in is 10 days.', () => {
    const gildedRose = new GildedRose([ new Item('Backstage passes to a TAFKAL80ETC concert', 10, 1) ]);
    const items = gildedRose.updateQuality();
    const added =items[0]
    expect(added.quality).to.equal(3);
  });

  it('should increase quality by 2 when sell in is 6 days.', () => {
    const gildedRose = new GildedRose([ new Item('Backstage passes to a TAFKAL80ETC concert', 6, 1) ]);
    const items = gildedRose.updateQuality();
    const added =items[0]
    expect(added.quality).to.equal(3);
  });

  // Test boundaries of range 1 to 5 day sell in
  it('should increase quality by 3 when sell in is 5 days.', () => {
    const gildedRose = new GildedRose([ new Item('Backstage passes to a TAFKAL80ETC concert', 5, 1) ]);
    const items = gildedRose.updateQuality();
    const added =items[0]
    expect(added.quality).to.equal(4);
  });

  it('should increase quality by 3 when sell in is 1 day.', () => {
    const gildedRose = new GildedRose([ new Item('Backstage passes to a TAFKAL80ETC concert', 1, 1) ]);
    const items = gildedRose.updateQuality();
    const added =items[0]
    expect(added.quality).to.equal(4);
  });

  // Test sell in day 0 sets quality to 0
  it('should set quality to 0 when sell in is 0.', () => {
    const gildedRose = new GildedRose([ new Item('Backstage passes to a TAFKAL80ETC concert', 0, 10) ]);
    const items = gildedRose.updateQuality();
    const added =items[0]
    expect(added.quality).to.equal(0);
  });

  it('should not change quality 0 when sell in is less than 0.', () => {
    const gildedRose = new GildedRose([ new Item('Backstage passes to a TAFKAL80ETC concert', -1, 0) ]);
    const items = gildedRose.updateQuality();
    const added =items[0]
    expect(added.quality).to.equal(0);
  });
})